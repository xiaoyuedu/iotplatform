# 物联网云平台

## 介绍
同时具有BAP（业务分析平台）和BMP（设备管理平台）的部分功能。

源码注释很详细，欢迎一同交流学习。

> 在线演示：<a href="http://graduation.zengjianqi.com" target="_blank">点击跳转</a>
>
> 账号：admin 密码：admin
>
> 扫描以下二维码同样可以在线体验
>
> <img src="illustration/网站二维码.png"/>

> 视频演示：<a href="https://www.bilibili.com/video/BV1uE411w77F/" target="_blank">点击跳转</a>
>
> 扫描以下二维码同样可以观看
>
> <img src="illustration/视频演示.png"  />



## 所用技术栈

### 前端

HTML/CSS、JavaScript、jQuery、AJAX、X-admin、Layui。

### 后端

PHP、MySQL、ThinkPHP5.0。

### 部署

LAMP。



## 安装教程

1.  配置LAMP/WAMP环境，并设置网站根目录为public目录。
2.  创建MySQL数据库并导入项目根目录下的data.sql数据库。
3.  配置application目录下的datebase.php文件中的相关属性。
4.  PHP版本7.2+。



## 效果图

### 登录模块

![登录页面](illustration/登录页面.png)

### 管理员账号管理模块

![管理员账号页面](illustration/管理员账号页面.png)

### 用户账号管理模块

![用户账号页面](illustration/用户账号页面.png)

### 设备信息总览模块

![设备信息总览页面](illustration/设备信息总览页面.png)

### 增删设备模块

![增删设备页面](illustration/增删设备页面.png)

### 设备绑定信息模块

![设备绑定信息页面](illustration/设备绑定信息页面.png)

### 设备实时动态模块

![设备实时动态](illustration/设备实时动态.jpg)

### 日志记录模块

![日志记录页面](illustration/日志记录页面.png)

### 数据总览模块

![数据总览](illustration/数据总览.jpg)



## 版权信息

本源码遵循GPL-3.0开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2006-2020 by 曾健起 (https://zengjianqi.com/)

All rights reserved。

更多细节参阅 [LICENSE](LICENSE)